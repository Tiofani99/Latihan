# Software Quality And Assurance

Software Quality Assurance adalah proses sistematis untuk memeriksa apakah sebuah
software telah dikembangkan sesuai dengan kebutuhan yang telah ditentukan sebelumnya. 
Proses ini, bisa dilaksanakan oleh seorang QA Tester atau oleh seorang QA Engineer

**Repository ini berisi tentang latihan-latihan matakuliah Software Quality And Assurance**

Link Heroku masih proses, eror terus deploynya, tadi aja salah commit :(


*  Buku referensi : 
https://www.obeythetestinggoat.com/book/praise.harry.html


*  **Untuk menjalankan gunakan perintah** <br>
`python manage.py runserver`



Untuk Project tiap latihan dapat diakses pada tautan berikut :

* [Latihan 1](https://github.com/Tiofani99/Testing-Static-Web)
* [Latihan 2](https://gitlab.com/Tiofani99/Latihan/-/tree/exercise_2)
* [Latihan 3](https://gitlab.com/Tiofani99/Latihan/-/tree/exercise_3)




# Tentang Excercise 1

Merupakan latihan dari bab 1-3

*  Unit Testing matematika sederhana

>     def test_bad_maths(self):
>         self.assertEqual(1 + 1, 3)


*  Unit testing untung mengecek html dan title
  
  ```
  def test_home_page_returns_correct_html(self):
        request = HttpRequest()  
        response = home_page(request)  
        html = response.content.decode('utf8')  
        self.assertTrue(html.startswith('<html>'))  
        self.assertIn('<title>To-Do lists</title>', html)  
        self.assertTrue(html.endswith('</html>')) 
```


# Tentang Exercise 2

Latihan 2 ini agak pusing karena sering sekali kodenya error dan tidak sesuai dengan
apa yang diminta dari buku. Pada latihan 3 kali ini juga sudah menggunakan database

`inputbox.send_keys('Tambah Lagi dong')`

Kode di atas yang digunakan untuk menambahkan to-do otomatis

Adapun untuk menambahkan komentar bisa menggunakan codingan di bawah untuk kurang dari 5

```
def test_comment_for_less_than_five_items(self):
        Item.objects.create(text='itemey 1')
        Item.objects.create(text='itemey 2')

        response = self.client.get('/')
        html = response.content.decode('utf8')
        comment = '<h4 id="comment">sibuk tapi santai</h4>'

        self.assertIn(comment, html)
```

Codingan berikut untuk to-do lebih dari 5

```
def test_comment_for_equal_or_more_than_five_items(self):
        Item.objects.create(text='itemey 1')
        Item.objects.create(text='itemey 2')
        Item.objects.create(text='itemey 3')
        Item.objects.create(text='itemey 4')
        Item.objects.create(text='itemey 5')
        Item.objects.create(text='itemey 6')

        response = self.client.get('/')
        html = response.content.decode('utf8')
        comment = '<h4 id="comment">oh tidak</h4>'
        self.assertIn(comment, html)
```

Kedua fungsi di atas terdapat pada test.py



# Tentang Excercise 3
Oke gaes pada latihan 3 kita belajar memperbaiki functional test dengan cara melakukan
isolasi gampangannya, udah kaya PDP sama ODP aja ya. Test Isolation merupakan proses
pemecahan suatu sistem/aplikasi menjadi modul-modul yang lebih kecil, sehingga lebih mudah 
ketika dievaluasi. Jenis testing seperti ini biasanya dilakukan ketika bug susah ditemukan
dalam suatu proyek aplikasi yang besar. Dengan menggunakan testing ini, programmer atau tester
dapat dengan mudah menemukan error.

Implementasi Test Isolation bisa dilihat dari perubahan desain dari latihan 2 .


1. Yang pertama terdapat perpindahan  dari functional_tests.py, yaitu dipindahkan ke 
direktori baru dengan nama functioanl_test/ dengan nama yang baru juga menjadi tests.py


**Sebelum** 

        .
    ├── db.sqlite3
    ├── functional_tests.py
    ├── lists
        ├── [...]
    ├── manage.py
    ├── superlists
        ├── [...]

**Sesudah**

        .
    ├── db.sqlite3
    ├── functional_tests
    │   ├── __init__.py
    │   └── tests.py
    ├── lists
        ├── [...]
    ├── manage.py
    ├── superlists
        ├── [...]


2. Mengganti parameter kelas FunctionalTest/NewVisitor dari unittest.Case menjadi LiveServerTestCase. 

**Sebelum**

```
import unittest
...
class NewVisitorTest(unittest.Case):
```

**Sesudah**

```
from django.test import LiveServerTestCase
...
class NewVisitorTest(LiveServerTestCase):
    ...
```

3. Jika ingin menjalankan *unit test* dan *functional test* secara bersamaan, jalankan perintah `py manage.py test'
4. Jika ingin menjalankan *unit test*, jalankan perintah `py manage.py test lists`
5. Jika ingin menjalankan *functional test* jalankan perintah `py manage.py test functional_tests`







